# SUCS Site
---
## Project Structure
<br>

| Directory | Description |
| --------- | ----------- |
| components | PHP components of the site, each drives a separate element - e.g. static content, the library, the help system, etc. |
| htdocs/css | Cascading StyleSheets |
| htdocs/files | Files for download (e.g. Help examples) |
| htdocs/images | Images used by the site skin |
| htdocs/js | JavaScript |
| htdocs/js/tinymce | Placeholder dir for TinyMCE, which should not itself be stored in SVN |
| htdocs/pictures | Images used in the content (e.g. /Help) |
| lib | Custom libraries used by the site - e.g. session library |
| plugins | Custom Smarty plugins used by the site - e.g. banana drawing function |
| static | Static content for the static content components (includes help) |
| static/fragments | Any HTML fragments used to make up parts of pages (e.g. secondary bars) |
| templates | Smarty templates |
| templates_c | Compiled Smarty templates - do not use this dir |