<?php
error_reporting(E_STRICT);
// Display execution time?
//$displaytime = TRUE;

// start the clock (this is just temporary cause I'm interested in how long all this takes)
if (@$displaytime) {
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$begintime = $time;
}

/* --------------------------------------------------------
    Settings
   -------------------------------------------------------- */

include("settings.php");
if (!(isset($base) && isset($preferred_hostname) && isset($dbname))) {
	echo "Website unconfigured";
	exit();
}

/* --------------------------------------------------------
    Libraries
   -------------------------------------------------------- */
// Include our custom error handling business
require("../lib/error.php");


// Include the Smarty templating engine
define('SMARTY_DIR', '/usr/share/php/smarty3/');
require("/usr/share/php/smarty3/Smarty.class.php");
$smarty = new Smarty();
$smarty->setTemplateDir($base."templates");
$smarty->setCompileDir($base."templates_c");
$smarty->addPluginsDir($base."plugins");

$smarty->assign("baseurl", $baseurl);

// Initialise the sucssite database conn
require("/usr/share/php/adodb/adodb.inc.php");
$DB = NewADOConnection('postgres9');
$DB->Connect('dbname='.$dbname.' user='.$dbuser);
$DB->SetCharSet('utf8');
$DB->SetFetchMode(ADODB_FETCH_ASSOC);

// Initialise the sucs database conn
$sucsDB = NewADOConnection('postgres9');
$sucsDB->Connect('dbname='.$sucsdbname.' user='.$dbuser);
$sucsDB->SetCharSet('utf8');
$sucsDB->SetFetchMode(ADODB_FETCH_ASSOC);

// Include the session library
require($base."lib/session.php");
$session = new Session;
$smarty->assignByRef("session", $session);


// include feedback form stuff
if ($session->loggedin) {
	include("../lib/page-feedback.php");
}

/* --------------------------------------------------------
    Debugging
   -------------------------------------------------------- */

// Turn on adodb debugging by uncommenting:
//$DB->debug = true;

// Turn on Smarty debugging by uncommenting:
//$smarty->assign("debug",TRUE);

// Should we display unexpected output from components?
$compdebug = TRUE;

/* --------------------------------------------------------
    Read Browser's settings
   -------------------------------------------------------- */

// Determine the language to serve pages in
$languagelist = explode(',', @$_SERVER['HTTP_ACCEPT_LANGUAGE']);
$language = array();
$language['code'] = "en";
/*
The following is hideously unscientific, but works for now. What really needs
to happen is the list get searched to see if cy is higher priority than en
*/

if ($languagelist[0]=="cy") $language['code'] = "cy";
$language['file'] = ".".$language['code'];
$language['db'] = "_".$language['code'];

if ($language['code']=="en") {
$language['file'] = "";
$language['db'] = "";
}

$smarty->assign("language", $language);


// SSL?
$ssl_path = @$_SERVER['REQUEST_URI'];
if (($n=strpos($ssl_path,"?"))!==FALSE) $ssl_path=substr($ssl_path,0,$n);
$ssl_url = "https://".$preferred_hostname.$ssl_path;
$smarty->assign("ssl_url", $ssl_url);

// Need to use ORIG_PATH_INFO in user homedirs
if (isset($_SERVER['PATH_INFO'])) {
	$pathinfo = $_SERVER['PATH_INFO'];
} else {
	$pathinfo = @$_SERVER['ORIG_PATH_INFO'];
}

// Determine which component to run
$pathlist = explode('/', parse_url($pathinfo,PHP_URL_PATH));
while (end($pathlist) === "") array_pop($pathlist);
$smarty->assignByRef("pathlist", $pathlist);
$path = '';
$query = "select * from pagemap where path='/' ";
$params = array();
foreach($pathlist as $item) {
	if ($item && $item != '/') {
		$query .= "or path=? ";
		$params[] = $path."/*";
		$path .= "/$item";
		$query .= "or path=? ";
		$params[] = $path;
	}
}

// Determine the path of the request
$smarty->assignByRef("path", $path);

$query .= "order by depth desc";
$pagemap = $DB->GetAll($query, $params);
//echo $query;
if (!$pagemap) $smarty->assign("error", $DB->ErrorMsg());
if (!$pagemap || count($pagemap)<1) {
	$smarty->assign("component", "Default");
	$component = "";
} else {
	$smarty->assign("component", $pagemap[0]['component']);
	$component = $pagemap[0];
}

if ($path == "") $path="/";

// Prep values for the components to use
$smarty->assign("title", "Set Me");
$smarty->assign("body", "Empty Body");

include($base."components/menu.php");
//include($base."components/search.php");

// Load the component
$comppath = $base."components/".$component['component'].".php";
$compoutput = "";
if (file_exists($comppath)) {
	ob_start();
	include($comppath);
	$compoutput = ob_get_contents();
	ob_end_clean();
} else {
	header("HTTP/1.1 404 Not Found");
	$smarty->assign("body", "Component ".$component['component']." not found");
}

// Pass error/warning/info messages to the template
$smarty->assign("user_messages", $messages);

// Render the results
if (!(isset($no_template)) || (!$no_template)) {
// Send appropriate Content-Type
	header('Content-Type:text/html; charset=UTF-8');


	$smarty->display("head.tpl");
	$smarty->display("index.tpl");
	if ($compdebug) {
		$smarty->assign("compoutput",$compoutput);
		$smarty->display("debug.tpl");
	}

// Last chance to stop the clock and still end up with valid HTML
if (@$displaytime) {
	$time = microtime();
	$time = explode(" ", $time);
	$time = $time[1] + $time[0];
	$endtime = $time;
	$totaltime = ($endtime - $begintime);
	$smarty->assign("totaltime", $totaltime);
}

	$smarty->display("foot".$language['file'].".tpl");
} else {
	echo $compoutput;
}

// Save any changes made to the session data
$session->save();


?>