<?
// lookup real names from sid's useing campus ldap
function lookupSID($sid) {
	$ds=ldap_connect("ccs-suld1.swan.ac.uk");
	$ldappw = file_get_contents("/etc/unildap.secret");
	$ldappw = trim($ldappw);
	ldap_bind($ds, "cn=SUCS-BIND,ou=ServiceAccount,o=SWANUNI",$ldappw );
	$sr=ldap_search($ds, "ou=students,ou=Swansea,o=swanuni", "uid=".$sid); 
	$info = ldap_get_entries($ds, $sr);
	ldap_unbind($ds);
	return ucwords(strtolower($info[0]['givenname'][0]." ".$info[0]['sn'][0]));
}

// lookup addresses from postcodes useing the univeritys website
function lookup_postcode($postcode = "")
{
        $url = "https://intranet.swan.ac.uk/common/postcodeLookup.asp?pCode=".$postcode;
        $referer = "https://intranet.swan.ac.uk/common/postcodeaddresslookup.asp";

        $req = curl_init($url);
        curl_setopt($req, CURLOPT_HEADER, false);
        curl_setopt($req, CURLOPT_REFERER, $referer);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($req, CURLOPT_SSLVERSION, 3);
        $page = curl_exec($req);
        curl_close($req);

        $scrape = explode("returnAddress(\"", $page);

        $addresses = array();
        for ($i = 1; $i < count($scrape); $i++) {
                if (preg_match("/^[^,\"].+?\"/", $scrape[$i], $address)) {
                        $addr = str_replace("<BR>\"", "", $address[0]);
                        array_push($addresses, str_replace("<BR>", ", ", $addr));
                }
        }
        return $addresses;
}
?>
