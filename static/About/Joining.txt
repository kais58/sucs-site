<h2>Who can join?</h2>
<ul>
<li>You <strong>must</strong> be a member of Swansea University Students' Union (<a href="http://swansea-union.co.uk/">SUSU</a>) to be able to join the society.</li>
</ul>
<h2>How much is it?</h2>
<ul>
<li>Membership is &pound;5 per academic year and includes the summer holiday.</li>
</ul>
<h2>What do I get?</h2>
<ul>
<li><strong><a href="../Community/Milliways/">Bulletin Board System</a></strong> - Our online notice board, where society and other issues are discussed, and where users can request information and help.</li>
<li><strong><a href="../About/Room/">Computer room</a></strong> with 24 hour access.</li>
<li><strong><abbr>DVD+RW</abbr>/<abbr>CDRW</abbr></strong> - Burn downloaded software or backup your personal files onto <abbr>DVD</abbr> or <abbr>CD</abbr>.</li>
<li><strong>E-mail address</strong> - Your own choice of <strong>username</strong> and therefore email address.</li>
<li><strong>Extra disk space</strong> - It is possible to access your society disk space from the majority of campus computers, as well as over the internet using Web Folders, any WebDAV client, <abbr>FTP</abbr> or <abbr>SFTP</abbr>.</li>
<li><strong><a href="../Games/">Games Server</a></strong> - Use our new, campus network-based games server to play <cite>Counter-Strike: Source</cite>, <cite>UT2004</cite> and more.</li>
<li><strong>Laptop internet hookup</strong> from our room, using the campus high speed connection.</li>
<li><strong><a href="..//Knowledge/Library/">Library</a></strong> - Borrow a textbook or two to brush up on your technical knowledge (We have many of the <abbr>CS</abbr> recommended texts).</li>
<li><strong>Printers</strong> - Make use of our laser printer. (200 free pages, 2p per page after)</li>
<li><strong>Program Advisory</strong> - All students can get some help with their programming tasks.</li>
<li><strong>Shell account</strong> - Access our server using <abbr>SSH</abbr>.</li>
<li><strong><cite>Subversion</cite> repository</strong> - Especially useful for 2nd years working on their group projects.</li>
<li><strong><cite>Trac</cite> project management</strong> - Uses this powerful system to manage your group projects.</li>
<li><strong><a href="https://sucs.org/webmail/">Webmail</a></strong> - Access your email from anywhere with a web browser.</li>
<li><strong>Website hosting</strong> - You can put up your own web pages for the world to see. PHP and PostgreSQL are available.</li>
</ul>
<h2>How do I join?</h2>
<h3>In person</h3>
<p>You can join in person by <a href="../About/Room/">visiting The Room</a> and catching a member of admin.</p>
<p>The Room is located on the <strong>ground floor of the Student Union Building</strong>. The door to the room is half way along the side which faces towards Fulton House. There are usually helpful people in the room around lunchtime/early afternoon.</p>
<h3>Online</h3>
<p>Membership can be purchased from the <a href="http://www.swansea-union.co.uk/mysociety/sucs/">Student Union</a> once logged in. You then need to make a note of your transaction number and go to <a href="../susignup">http://sucs.org/susignup</a></p>
<p>Alternatively, fill in the form below and a member of admin will get in touch with you:</p>
<form action="/About/JoinEmail" method="post">
<div class="row">
		<label for="realname">Full name</label>
		<span class="textinput"><input id="realname" name="realname" size="40" type="text" /></span>
	</div>
<div class="row">
		<label for="student_number">Student number</label>
		<span class="textinput"><input id="student_number" maxlength="6" name="student_number" size="8" type="text" /></span>
	</div>
<div class="row">
		<label for="email">Email address</label>
		<span class="textinput"><input id="email" name="email" size="40" type="text" /></span>
	</div>
<div class="row">
		<label for="uname">Preferred username</label>
		<span class="textinput"><input id="uname" name="uname" size="8" type="text" /><br />
<div class="note">If you already have a Signup Slip, please do not use this form but go to the <a href="../signup/">signup page</a> instead.</div>
</span>
	</div>
<div class="row">
		<span class="textinput"><input id="submit" name="submit" type="submit" value="Send Request" />
		<input id="reset" name="reset" type="reset" value="Clear Form" /></span>
	</div>
<div class="clear"></div>
<h4>Notes</h4>
<ol>
<li>Please note that you <strong>must have</strong> a Swansea University student card and number to become a member of SUCS.</li>
<li>We regret that we cannot allow anyone who is not a member of Swansea University Student Union to become a member.</li>
</ol>
</form>