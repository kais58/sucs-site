<h2>Swansea University Computer Society</h2>
<p>SUCS is one of the university's longest-running and largest societies. In
addition to providing members with a range of <a href="About">IT
services</a>, we hold regular events and socials throughout the year.</p>
<p>We have our own <a href="About/Room">computer room</a> on campus with 24
hour access. There are usually members present through the week
so feel free to stop by.</p>
<p>Visit the <a href="Community">Community</a> section of the site for more
ways to get in touch with your fellow members, including <a href="Community/Milliways">Milliways</a>, our chat room.&nbsp;</p>
<p>SUCS celebrated its twenty-fifth birthday in the summer of 2014. We have compiled a site about the society's history. To read about it and contribute your part of the story, visit <a href="http://history.sucs.org/">http://history.sucs.org/</a>.</p>
<div style="text-align: center"><a href="http://history.sucs.org/"><img alt="SUCS history" height="50" src="https://sucs.org/images/sucshistory.png" width="231" /></a></div>