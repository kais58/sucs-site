<h3>Unreal Tournament</h3>
<p>To play Unreal Tournament <a href="https://games.sucs.org/auth/">log in</a> to the SUCS games system, then connect to games.sucs.org:17777 in UT.</p>

<p>The server requires that you have at least version 432 installed. Latest (community maintained) <a href="http://www.utpg.org/">patch is available here</a> for Windows and Linux</p>