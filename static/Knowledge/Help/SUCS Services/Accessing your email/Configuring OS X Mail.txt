<p>Open the <span style="font-weight: bold;">Mail</span> program by clicking on its icon in the Dock:</p>

<p><img width="141" vspace="0" hspace="0" height="77" border="0" align="bottom" alt="[OS X Mail]" src="/pictures/osxmailicon.jpg" /></p>

<p>If it is the first time you have run Mail on this computer, a wizard will appear and you will be prompted for your email settings immediately.</p>

<p><img src="/pictures/osxmail1.jpg" /></p>

<p>If this is the case, <a href="#entersettings">skip to the next step</a>.</p>

<p>If you already have accounts in <span style="font-weight: bold;">Mail</span>, you will not be prompted to create an account. Instead, go to the Mail menu, and choose Preferences:</p>

<p><img src="/pictures/osxmailmenu.jpg" /></p>

<p>The preferences window will open. Make sure that &quot;Accounts&quot; is selected at the top.</p>

<p><img src="/pictures/osxmailaccounts.jpg" /></p>

<p>Click the &quot;+&quot; button at the bottom-left of the window. This will open the &quot;New Account&quot; wizard.</p>

<p><a id="entersettings"></a><img src="/pictures/osxmail2.jpg" /></p>

<p align="baseline">You will be asked for a name for the account (e.g. &quot;SUCS Account&quot;), your full name and your email address. This will be of the form <span style="font-style: italic;">[username]</span>@sucs.org. Once you have entered these, click &quot;Continue&quot;.</p>

<p><img src="/pictures/osxmail3.jpg" /></p>

<p align="baseline">Then you will be asked for the details of the incoming mail server. When you click &quot;Continue&quot;, Mail will test the server's connection.</p>

<p><img src="/pictures/osxmail4.jpg" /></p>

<p>At this point, it will fail and you should click &quot;Continue&quot; again.</p>

<p><img src="/pictures/osxmail5.jpg" /></p>

<p>On the <span style="font-weight: bold;">Incoming Mail Security</span> page, you should tick &quot;Use Secure Socket Layer (SSL)&quot; and click &quot;Continue&quot;.</p>

<p><img src="/pictures/osxmail6.jpg" /></p>

<p>The <span style="font-weight: bold;">Outgoing Mail Server</span> can be set to sucs.org, using port 25, and &quot;SSL&quot;. Click &quot;Continue&quot;.</p>

<p><img src="/pictures/osxmail7.jpg" /></p>

<p>You will be presented with a summary of the settings you have entered. Click &quot;Continue&quot; again.</p>

<p><img src="/pictures/osxmail8.jpg" /></p>

<p>Click &quot;Done&quot; and you will be taken into the Mail program.</p>

<p><img src="/pictures/osxmailapp.jpg" /></p>

<p>Click &quot;Get Mail&quot; to retrieve your mail. By default, <span style="font-weight: bold;">Mail</span> will fetch your email every five minutes.</p>