<p>The Swansea University Computer Society provides the following main services to its members.  
</p>
<dl> 
<dt><a href="https://sucs.org/webmail/">Electronic Mail</a></dt> 
<dd>Fed up of being a number? Each society member gets an email address of the form <em>membername</em>@sucs.org. You can access this <a href="https://sucs.org/webmail/">via the web</a> or from mail clients supporting pop3 or imap (that's most of them).
<p>Learn more about <a href="../../Help/SUCS%20Services/Accessing%20your%20email">society e-mail</a>  </p>
</dd>
<dt>Additional Disk Space</dt> 
<dd>Society members have access to additional disk space, either from the  society machines or by WebDAV as a network folder on the LIS Windows machines, or your home system.
<p>Learn more about <a href="../../Help/SUCS%20Services/Using%20WebDAV">accessing disk space</a> </p>
</dd>  
<dt><a href="../../Services/Library">Reference Library</a></dt> 
<dd>The computer society has its own small library of fifty bought and donatedbooks available to members of the society.
<p>Learn more about <a href="../../Services/Library">the books available</a> </p>
</dd>  
<dt><a href="../../Services/Room">Computer Room</a></dt> 
<dd>The society has its own small room underneath the union building which has Linux desktop systems available to members twenty four hours a day, as well as connection points for personal laptops. Use of the room is governed by  <a href="../../Info/Room%20Rules">the room rules</a>. In particular please note that laptops must be registered with the society to be used so that anyone misusing the system can be identified and disciplined. University computing regulations also apply at all times.
<p>Learn more about <a href="../../Info/Room%20Rules">the room usage rules</a></p>
</dd>  
<dt>Personal Webspace</dt> 
<dd>If you create and place files in a public_html folder they will be visible on the web at http://sucs.org/~<em>username</em>. The index or default web page is called "index.html". For the more adventurous web designers, <a href="http://php.net/">PHP scripting</a> and <a href="http://postgresql.org">PostgreSQL databases</a> are available.</dd><dt><br /></dt>  
<dt><a href="../../Help/Program%20Advisory">Program Advisory</a></dt> 
<dd>Assistance with computing related problems provided by members to members. If you need help or can provide help see  <a href="../../Help/Program%20Advisory">the advisory page</a>.</dd><dt><br /></dt>  
<dt>Web Proxy</dt> 
<dd>The society operates a caching web proxy for the machines in the room and any laptops using the laptop ports. Use of the proxy is compulsory and web access is not available by other means.
<p>The proxy is <strong>proxy.sucs.org:3128</strong></p>
</dd>  
<dt><a href="../../Services/Desktop%20on%20Demand">Desktop On Demand</a></dt> 
<dd>Access a computer society Linux desktop anywhere that has a web browser supporting java.
<p>Learn more about <a href="../../Help/SUCS%20Services/Using%20Desktop%20on%20Demand">Desktop on Demand</a></p>
</dd>  
<dt><a href="../../mailman/listinfo">Mailing lists</a></dt> 
<dd>SUCS provides student societies with mailing lists which they can use to make announcements or chat.
<p>Learn about <a href="../../Help/SUCS%20Services/Using%20mailing%20lists">subscribing to and using mailing lists</a> and <a href="../../Help/SUCS%20Services/Administering%20a%20mailing%20list">administering them</a></p>
</dd>  
<dt><a href="../../Services/Milliways">Milliways</a></dt> 
<dd>SUCS has its own homebrewed chat system called Milliways.
<p>Learn more about <a href="../../Help/SUCS%20Services/Using%20Milliways">Milliways</a></p>
</dd>  
<dt>Jabber server</dt> 
<dd>Fed up with MSN spam? Switch to <a href="http://www.jabber.org/">Jabber</a> instead.
<p>Learn more about <a href="../../Help/SUCS%20Services/Jabber%20Server%20HOWTO">Jabber</a></p>
</dd> </dl>  
<hr />
<p> SUCS would like to thank <a href="http://sucs.org/~rohan">Steve Whitehouse</a>, <a href="http://sucs.org/~rhys">Rhys Jones</a>, <a href="http://sucs.org/~dez">Denis Walker</a> and Alan Cox for their contribution to these Help pages.</p>