<div class="box">
<div class="boxhead">
<h2>Edmund Smith (edmund) - "Helicopters are Cool"</h2>
</div>
<div class="boxcontent">
<div id="player">
<object data="//sucs.org/videos/talks/mediaplayer.swf?file=2012-10-18/edmund.flv" height="480" id="player" type="application/x-shockwave-flash" width="600">
<param name="height" value="480" />
<param name="width" value="600" />
<param name="file" value="/videos/talks/2012-10-18/edmund.flv" />
<param name="image" value="/videos/talks/2012-10-18/edmund.png" />
<param name="id" value="player" />
<param name="displayheight" value="480" />
<param name="FlashVars" value="image=/videos/talks/2012-10-18/edmund.png" />
</object>
</div>
<p><strong>Length: </strong>17m 33s</p>
<p><strong>Video: </strong><a href="/videos/talks/2012-10-18/edmund.flv" title="600x480 MPEG-4 AVC - 86MB">600x480</a> (MPEG-4 AVC, 86MB)</p>
</div>
<div class="boxfoot">
<p>&nbsp;</p>
</div>
</div>
