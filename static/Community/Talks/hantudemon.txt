<div class="box">
<div class="boxhead">
<h2>Joseph Bhart (hantudemon) - "Genomics"</h2>
</div>
<div class="boxcontent">
<ul>
<li><a href="http://sucs.org/videos/talks/2011-11-01/hantudemon.pdf">Download the slides</a></li>
</ul>
<div id="player">
<object data="http://sucs.org/videos/talks/mediaplayer.swf?file=2011-11-01/hantudemon.flv" height="275" id="player" type="application/x-shockwave-flash" width="320">
<param name="height" value="256" />
<param name="width" value="320" />
<param name="file" value="/videos/talks/2011-11-01/hantudemon.flv" />
<param name="image" value="/videos/talks/2011-11-01/hantudemon.png" />
<param name="id" value="player" />
<param name="displayheight" value="256" />
<param name="FlashVars" value="image=/videos/talks/2011-11-01/hantudemon.png" />
</object>
</div>
<p><strong>Length: </strong>14m 4s</p>
<p><strong>Video: </strong><a href="http://sucs.org/videos/talks/2011-11-01/hantudemon.ogv" title="720x576 Ogg
Theora - 25MB">720x576</a> (Ogg Theora, 25MB)</p>
</div>
<div class="boxfoot">
<p>&nbsp;</p>
</div>
</div>