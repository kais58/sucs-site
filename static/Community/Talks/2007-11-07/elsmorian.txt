<div class="box">
	<div class="boxhead"><h2>Chris Elsmore (elsmorian) - Digital Audio: What Happened After Music, Media, and DRM</h2></div>
	<div class="boxcontent">
	<p>Following on from his previous talk on Music, Media, and DRM, Chris provides an introduction to the fundamentals of Digital Audio.</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-11-07/2007-11-07-elsmorian.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-11-07/2007-11-07-elsmorian.flv" image="/videos/talks/2007-11-07/2007-11-07-elsmorian.png" id="player" displayheight="240"><param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-11-07/2007-11-07-elsmorian.flv" />
<param name="image" value="/videos/talks/2007-11-07/2007-11-07-elsmorian.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-11-07/2007-11-07-elsmorian.png" />
</object></div>

<p><strong>Length: </strong>19m 21s</p>
<p><strong>Video:</strong>Coming Soon (H.264 .mov)</p>
<p><strong>Slides:</strong>Coming Soon (PDF)<br /></p>
</div>
	<div class="boxfoot"><p>&nbsp;</p></div>
</div>
