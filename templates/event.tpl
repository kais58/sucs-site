{if $editable}
	<div style="float: right">
	<a href="{$baseurl}/Community/Events/{$event.category}_{$event.id}?action=edit">(Edit)</a>
	</div>
{/if}
        
<div>
	<h2 style="text-align:left;display:inline">What?</h2> 
	<h1 style="text-align:right;display:inline">{$event.name}</h1>
</div>
<br>
<div>
	<h2 style="text-align:left;display:inline">Where?</h2>
	<h1 style="text-align:right;display:inline">{$event.location}</h1>
</div>
<br>
<div>
	<h2 style="text-align:left;display:inline">When?</h2>
	<h1 style="text-align:right;display:inline">{$event.whn|date_format:"l \t\h\e jS \o\f F Y \a\t h:i:s A"}{if $event.show_time},{$event.whn_timestamp|date_format:"%H:%M"}{/if}</h1>
</div>

<h1>Why?</h1>

<p1>
{$event.description}
</p1>