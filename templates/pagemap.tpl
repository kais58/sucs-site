{if $html == TRUE}
The Pagemap<br/>
<table style="border:1px">
<tr>
<th>Depth</th>
<th>Path</th>
<th>Component</th>
<th>&nbsp;</th>
</tr>
{foreach name=map from=$map key=key item=item}
<tr>
<td>{$item.depth}</td>
<td>{$item.path}</td>
<td>{$item.component}</td>
<td>
<form action="{$path}/Edit" method="POST" style="display:inline;">
<input type="hidden" name="mapid" value="{$item.id}" />
<input type="submit" name="submit" value="Edit" />
</form>
</td>
</tr>
{/foreach}
</table>
{else}
	<div class="errorbar">
		<div><div><div>
			You do not have access to this component.
		</div></div></div>
	</div>
{/if}