<div id="punwrap">
<div id="punForum" class="pun">
<div id="idx1" class="blocktable">
     <h2><span>Milliways Folder List</span></h2>

<div class="box">
<table cellspacing="0">
<thead>
<tr>
<th scope="col" class="tc1">Forum</th>
<th scope="col" class="tc2">Topics</th>
<th scope="col" class="tc3">Posts</th>
<th scope="col" class="tcr">Last post</th>
</tr>
</thead>
<tbody>
{foreach from=$folders item=fol}
<tr>
<td class="tcl">
 <div class="intd">
  <div class="icon"><div class="nosize"><!-- --></div></div>
  <div class="tclcon">
   <h3><a href="{$path}/{$fol.name|escape:'url'}">{$fol.name|escape:'html'}</a></h3>
   {$fol.topic|escape:'html'}
  </div>
 </div>
</td>
<td class="tc2">{$fol.topics}</td>
<td class="tc3">{$fol.last-$fol.first}</td>
<td class="tcr">{$fol.lastpost|date_format}</td>
</tr>
{/foreach}
</tbody>
</table>
</div>
</div>
</div>
</div>
