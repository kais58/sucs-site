<table>
<tr>
 <td nowrap="nowrap" class="date" colspan="3">
Year: {foreach from=$yearlist item=year}{if $year != $thisyear}<a href="{$mwpath}/uri/{$year}-01-01">{$year}</a> {else}{$year} {/if}{/foreach}
 </td>
</tr>
<tr>
 <td nowrap="nowrap" class="date" colspan="3">
Month: {foreach from=$monthlist key=num item=month}{if $num != $thismonth}<a href="{$mwpath}/uri/{$thisyear}-{$num}-01">{$month}</a> {else}{$month} {/if}{/foreach}
 </td>
</tr>
<tr>
 <td nowrap="nowrap" class="date" colspan="3">
Day: {foreach from=$daylist item=day}{if $day != $thisday}<a href="{$mwpath}/uri/{$thisyear}-{$thismonth}-{$day}">{$day}</a> {else}{$day} {/if}{/foreach}
 </td>
</tr>
{assign var=lastday value=''}
{foreach from=$urilist item=row name=list}
{if stristr($row.flags,"sucs")===FALSE || $session->loggedin}
{if $row.day != $lastday}
{assign var=lastday value=$row.day}
<tr>
 <td nowrap="nowrap" class="date" colspan="3">{$row.added|date_format:'%d %b %Y'}</td>
</tr>
{/if}
{assign var=rows value=1}
{if $row.title}{assign var=rows value=$rows+1}{/if}
{if count($row.taglist)}{assign var=rows value=$rows+1}{/if}
<tr>
 <td nowrap="nowrap" class="time" rowspan="{$rows}">{$row.hour}</td>
 <td nowrap="nowrap" class="user" rowspan="{$rows}">{if stristr($row.flags,"anon")===FALSE}{$row.user}{else}<i>Anonymous</i>{/if}</td>
 <td ><a href="{$row.url|escape:"hexentity"}">{$row.url|truncate:80:" ... ":true:true|escape:"html"}</a>{if stristr($row.flags,"nsfw")!==FALSE} (NSFW){/if}</td>
</tr>
{if $row.title}
<tr><td>{$row.title|escape:"hexentity"}</td></tr>
{/if}
{if count($row.taglist)}
<tr>
 <td class="taglist">
 Tags: 
{foreach from=$row.taglist item=tcount key=tname name=tags}{$tname}{if !$smarty.foreach.tags.last}, {/if}{/foreach}
 </td>
</tr>
{/if}
{/if}
{/foreach}
</table>
