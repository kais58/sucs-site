{if count($users) > 0 }
<div class="cbb">

<h3>Recent site visitors</h3>

<table width="100%" class="border">
<tr>
<th width="70%">Username</th>
<th width="30%">Idle for</th>
</tr>
{foreach name=users from=$users item=user}
<tr>
	<td><a href="{$baseurl}/Community/Members/{$user.username|escape:'url'}">{$user.username}</a></td>
	<td>{$user.idle}</td>
</tr>
{/foreach}
</table>
</div>
{/if}
