
<div class="box">
	<div class="boxhead"><h2>{$book.title}</h2></div>
	<div class="boxcontent">

		<strong>{if $book.onloan}On loan{else}Book Available{/if}</strong>
		{if $book.image_url != ""}<img class="emblem" src="{$book.image_url|escape}" alt="{$book.title|escape}" />{/if}
		<p>Author: {$book.author}</p>
	{if isset($book.description)}
		<div>{$book.description}</div>
	{/if}
{if $editable == true}
	<div class="edit">
	<ul><li><a href="?action=edit">Edit</a></li></ul>
	</div>
{/if}
	<div class="clear"></div>
	</div>
	<div class="hollowfoot"><div><div></div></div></div>
</div>
