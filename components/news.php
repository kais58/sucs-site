<?php
// Ensure we have a trailing slash so that relative links within the page work
if ($_SERVER['PATH_INFO']=="/News") header("Location: ".$baseurl."/News/");

// A user must be in the staff group in order to edit content in this component
$permission = "sucsstaff";
$newstable = "news";

if (isset($session->groups[$permission])) {
// display Edit links on page
	$smarty->assign("editable", TRUE);
	switch (@$_REQUEST['action']) {
	case "Save":
		$record['title'] = $_REQUEST['edittitle'];
		$record['body'] = $_REQUEST['body'];
		if (isset($_REQUEST['sticky']))
			$record['sticky'] = 't';
		else
			$record['sticky'] = 'f';

		if ($_REQUEST['id']=="*") {
			$record['author'] = $session->username;
			$record['date'] = "now";
			$DB->AutoExecute($newstable, $record, 'INSERT');
		} else {
			$DB->AutoExecute($newstable, $record, 'UPDATE', "id=".$DB->qstr($_REQUEST['id']));
		}
		include("motd.php");

		break;
	case "Delete":
		$deleted = $DB->Execute("DELETE FROM $newstable WHERE id=?", array($_REQUEST['id']));
		include("motd.php");
		break;
	case "Expire":
		$expired = $DB->Execute("UPDATE $newstable SET expiry=now() WHERE id=?", array($_REQUEST['id']));
		include("motd.php");
		break;
	}
}

$query = "SELECT *, date_part('epoch', expiry) AS expirytime FROM news";
if (@$pathlist[2]!="") $query .= " WHERE title=".$DB->qstr($pathlist[2]);
$query .= " ORDER BY date DESC";

$smarty->assign("news", $DB->GetArray($query));

$output = $smarty->fetch("news.tpl");
$smarty->assign("title", "News");

if (isset($session->groups[$permission])) {
// Perform any requested actions
	switch (@$_REQUEST['action']) {
	case "create":
		$news = array(array("id" => "*"));
		$smarty->assign("news", $news);
	case "edit":
		$output = $smarty->fetch("news_edit.tpl");
		$smarty->assign("action", "edit");
		$smarty->assign("title", "Edit News");
		break;
	case "delete-query":
		$output = $smarty->fetch('news_delete.tpl').$output;
		break;
	case "expire-query":
		$output = $smarty->fetch('news_expire.tpl').$output;
		break;
	case "Delete":
		if ($deleted) $output = "News item deleted";
		break;
	}
}

$smarty->assign("body", $output);
?>
