<?php

function parseIdleTime($idletime) {
	$idlearray = preg_split("/[hms]/",$idletime);
	if (strstr($idletime,'h')) {
		$hours = $idlearray[0];
		$minutes = $idlearray[1];
		$seconds = 0;
	} elseif (strstr($idletime,'m')) {
		$hours = 0;
		$minutes = $idlearray[0];
		$seconds = $idlearray[1];
	} else {
		$hours = 0;
		$minutes = 0;
		$seconds = $idlearray[0];
	}

	return (intval($hours * 3600)) + (intval($minutes) * 60) + intval($seconds);

}

function compare_idletime($a, $b) {
	return strnatcmp($a['idleseconds'], $b['idleseconds']);
}

$mwpath = array_search("Milliways", $pathlist);

$newpath = $baseurl;
for($i=0;$i<=$mwpath;$i++) {
	if ($i!=0) $newpath .= "/";
	$newpath .= $pathlist[$i];
}
$smarty->assign("mwpath", $newpath);

$cal = cal_info(CAL_JULIAN);

if (strtolower($pathlist[$mwpath+1]) == "uri") {
	$latest = FALSE;
	$today = FALSE;
	unset($search);
	if (isset($pathlist[$mwpath+2])) {
		if ($pathlist[$mwpath+2] == "search" && isset($pathlist[$mwpath+3]))  {
			$search = $pathlist[$mwpath+3];

		} else {
			$today = strtotime($pathlist[$mwpath+2]);
		}
	}
	if ($today === FALSE && !isset($search)) {
		$today = time();
		$latest = TRUE;
	}

	$smarty->assign("extra_scripts", array(
		'<meta name="ROBOTS" content="NOINDEX" />',
		'<meta name="ROBOTS" content="NOFOLLOW" />'));

	$mwdb = new PDO('sqlite:/var/lib/mw/mwuri.db');
	$res = $mwdb->prepare("select distinct strftime('%Y',added) from mwuri");
	$res->execute();
	$yearlist = $res->fetchAll(PDO::FETCH_COLUMN,0);
	$smarty->assign("yearlist",$yearlist);

	$res= $mwdb->prepare("select distinct strftime('%m',added) from mwuri where strftime('%Y',added) = ?");
	$res->execute(array(date("Y",$today)));
	$mlist = $res->fetchAll(PDO::FETCH_COLUMN,0);
	$monthlist = array();
	foreach( $mlist as $m) {
		$monthlist[$m] = $cal['abbrevmonths'][(int)$m];
	}
	$smarty->assign("monthlist",$monthlist);

	$res= $mwdb->prepare("select distinct strftime('%d',added) from mwuri where strftime('%Y-%m',added) = ?");
	$res->execute(array(date("Y-m",$today)));
	$daylist = $res->fetchAll(PDO::FETCH_COLUMN,0);
	$smarty->assign("daylist",$daylist);

	if ($today === FALSE && isset($search)) {
		// do a search
		$res = $mwdb->prepare("select *, time(added,'localtime') as hour, date(added,'localtime') as day from mwuri where title like ? or url like ? order by added desc");
		$term = "%".$search."%";
		$res->execute(array($term,$term));
		$data = $res->fetchAll(PDO::FETCH_ASSOC);
	} else {
		//standard date based lookup
		$res = $mwdb->prepare("select *, time(added,'localtime') as hour, date(added,'localtime') as day from mwuri where date(added,'localtime')=? order by added desc");
		$res->execute(array(date("Y-m-d",$today)));
		$data = $res->fetchAll(PDO::FETCH_ASSOC);
		if ($latest && count($data == 0)) {
			$day = end($yearlist)."-".end($monthlist)."-".end($daylist);
			$today = strtotime($day);
			$res->execute(array(date("Y-m-d",$today)));
			$data = $res->fetchAll(PDO::FETCH_ASSOC);
		}
	}
	foreach($data as $k => $row)
	{
		if ($row['tags']) {
			$json = json_decode($row['tags'], TRUE);
			if (is_array($json))
				$data[$k]['taglist'] = $json[0]['top_tags'];
		}
	}
	$smarty->assign("urilist", $data);

	$smarty->assign("thisyear", date("Y",$today));
	$smarty->assign("thismonth", date("m",$today));
	$smarty->assign("thisday", date("d",$today));

	$output = $smarty->fetch($base."templates/milliways_uri.tpl");
	$smarty->assign("title", "Milliways URI");
	$smarty->assign("body", $output);
	$smarty->assign("extra_styles", "/css/milliways.css");
	$smarty->assign("refresh", 120);
	return;
}

if (strtolower($pathlist[$mwpath+1]) == "status") {
	$today = FALSE;
	$wantuser = FALSE;
	if (isset($pathlist[$mwpath+2])) {
		if ($pathlist[$mwpath+2] == "user" && isset($pathlist[$mwpath+3])) 
			$wantuser = $pathlist[$mwpath+3];
		else
			$today = strtotime($pathlist[$mwpath+2]);
	}
	if ($today === FALSE)
		$today = time();

	$smarty->assign("extra_scripts", array(
		'<meta name="ROBOTS" content="NOINDEX" />',
		'<meta name="ROBOTS" content="NOFOLLOW" />'));

	$mwdb = new PDO('sqlite:/var/lib/mw/mwuri.db');
	if ($wantuser !== FALSE) {
		$res = $mwdb->prepare("select *, time(added,'localtime') as hour, date(added,'localtime') as day from mwdoing where user=? order by added desc");
		$res->execute(array($wantuser));
	} else {
		$res = $mwdb->prepare("select *, time(added,'localtime') as hour, date(added,'localtime') as day from mwdoing where strftime('%Y-%m',added,'localtime')=? order by added desc");
		$res->execute(array(date("Y-m",$today)));
	}
	//$res = $mwdb->prepare("select *, time(added,'localtime') as hour, date(added,'localtime') as day from mwdoing where date(added,'localtime')=? order by added desc");
	//$res->execute(array(date("Y-m-d",$today)));
	$data = $res->fetchAll(PDO::FETCH_ASSOC);
	$smarty->assign("statuslist", $data);

	$res = $mwdb->prepare("select distinct strftime('%Y',added) from mwdoing");
	$res->execute();
	$yearlist = $res->fetchAll(PDO::FETCH_COLUMN,0);
	$smarty->assign("yearlist",$yearlist);

	$res= $mwdb->prepare("select distinct strftime('%m',added) from mwdoing where strftime('%Y',added) = ?");
	$res->execute(array(date("Y",$today)));
	$mlist = $res->fetchAll(PDO::FETCH_COLUMN,0);
	$monthlist = array();
	foreach( $mlist as $m) {
		$monthlist[$m] = $cal['abbrevmonths'][(int)$m];
	}
	$smarty->assign("monthlist",$monthlist);

	$res= $mwdb->prepare("select distinct strftime('%d',added) from mwdoing where strftime('%Y-%m',added) = ?");
	$res->execute(array(date("Y-m",$today)));
	$daylist = $res->fetchAll(PDO::FETCH_COLUMN,0);
	$smarty->assign("daylist",$daylist);

	$smarty->assign("thisyear", date("Y",$today));
	$smarty->assign("thismonth", date("m",$today));
	$smarty->assign("thisday", date("d",$today));

	$output = $smarty->fetch($base."templates/milliways_status.tpl");
	$smarty->assign("title", "Milliways Status");
	$smarty->assign("body", $output);
	$smarty->assign("extra_styles", "/css/milliways.css");
	$smarty->assign("refresh", 120);
	return;
}

if (strtolower($pathlist[$mwpath+1]) == "tag") {
	$smarty->assign("extra_scripts", array(
		'<meta name="ROBOTS" content="NOINDEX" />',
		'<meta name="ROBOTS" content="NOFOLLOW" />'));

	$mwdb = new PDO('sqlite:/var/lib/mw/mwuri.db');
	if (isset($pathlist[$mwpath+2])) {
	$res = $mwdb->prepare("select *, date(added) as day, substr(tag,2) as name from mwtag where tag=? order by added desc");
	$res->execute(array('#'.$pathlist[$mwpath+2]));
	$data = $res->fetchAll(PDO::FETCH_ASSOC);
	$smarty->assign("taglist", $data);
	$output = $smarty->fetch($base."templates/milliways_tag.tpl");
	} else {
	$res = $mwdb->prepare("select distinct tag, substr(tag,2) as name, count(tag) as count from mwtag group by tag order by count(tag) desc limit 20");
	$res->execute();
	$data = $res->fetchAll(PDO::FETCH_ASSOC);
	$smarty->assign("toplist", $data);

	$res = $mwdb->prepare("select distinct tag, substr(tag,2) as name, count(tag) as count, max(added) as dated, date(max(added)) as day from mwtag group by tag order by max(added) desc limit 20");
	$res->execute();
	$data = $res->fetchAll(PDO::FETCH_ASSOC);
	$smarty->assign("latestlist", $data);

	$output = $smarty->fetch($base."templates/milliways_taglist.tpl");
	}

	$smarty->assign("title", "Milliways Tags");
	$smarty->assign("body", $output);
	$smarty->assign("extra_styles", "/css/milliways.css");
	$smarty->assign("refresh", 120);
	return;
}

exec("/usr/bin/mw -who", $wholist, $ret);
$people = array();
$idlers = array();
foreach ($wholist as $person) {
    $pid = strtok($person, " ");
    if ($pid == "" || $pid == "Name" || substr($pid,0,5)=="-----") continue;
    $person = array(
	"username" => trim(substr($person, 1, 16)),
	"idle"     => trim(substr($person, 18, 6)),
	"idleseconds" => parseIdleTime(trim(substr($person, 18, 6))),
	"what"     => substr($person,25));

	if ($person['idleseconds'] > 10800) {
		$idlers[] = $person;
	} else {
		$people[] = $person;
	}
}

usort($people, 'compare_idletime');
usort($idlers, 'compare_idletime');

$output .= print_r($pathlist, true);

// summary lists
$mwdb = new PDO('sqlite:/var/lib/mw/mwuri.db');
$res = $mwdb->prepare("select *, time(added,'localtime') as hour, date(added,'localtime') as day from mwuri order by added desc limit 5");
$res->execute();
$urilist = $res->fetchAll(PDO::FETCH_ASSOC);
foreach ($urilist as $k=>$v) {
	if ($v['title'] == "") $urilist[$k]['title']=$v['url'];
	$urilist[$k]['title']=filter_var(trim($urilist[$k]['title']),FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
}
$smarty->assign("urilist", $urilist);

$res = $mwdb->prepare("select distinct tag, substr(tag,2) as name, count(tag) as count, max(added) as dated, date(max(added)) as day from mwtag group by tag order by max(added) desc limit 5");
$res->execute();
$taglist = $res->fetchAll(PDO::FETCH_ASSOC);
$smarty->assign("taglist", $taglist);

$smarty->assign("people",$people);
$smarty->assign("idlers",$idlers);
$output = $smarty->fetch($base."templates/milliways.tpl");
$smarty->assign("title", "Milliways");
$smarty->assign("body", file_get_contents($base."static/fragments/Milliways.txt"));
$smarty->assign("secondary", $output);
$smarty->assign("refresh", 120);
?>
